import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

//import './main.html';
import '../imports/viewmodel/kardex'
import '../imports/viewmodel/layout'
import '../imports/viewmodel/accounts'
import 'jquery-serializejson'
import swal from 'sweetalert'
