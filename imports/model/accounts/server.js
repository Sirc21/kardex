import {users} from './class'
Meteor.methods({
    newUser : function(form){
        let profile = {
            firstName:form.name,
            LastName:form.lastname,
            cargo:form.cargo
        }
        var id; 
        id = Accounts.createUser({
            username:form.username,
            email: form.email,
            password: form.password,
            profile:profile
        });
        return id
    },
       /* if (pushRoles.length > 0) {
            Roles.addUsersToRoles(id, pushRoles);
        }
    },
    editUser : function(user,form,arrayRoles){
        let profile = {
            ci:form.ci,
            firstName:form.firstName,
            fatherLastName:form.fatherLastName,
            motherLastName:form.motherLastName,
            birthday:form.birthday
        }

        Meteor.users.update(user,{
            $set: {
                "username":form.username,
                "emails":[{address:form.email,verified:false}],
                "profile":profile,
                "roles":[],
            }
        });
        if (arrayRoles.length > 0) {
            Roles.addUsersToRoles(user, arrayRoles);
        }
        if(form.password != ''){
           Accounts.setPassword(user, form.password)
        }
    },
    searchUser:function(parameters){ // buscador
        if(parameters != null){
            var query = 
            [
                {$project:{
                    _id: {
                        $toString: "$_id"
                    },
                    names:{
                        $concat:['$profile.ci',' ','$profile.firstName',' ','$profile.fatherLastName',' ','$profile.motherLastName']
                    }
                }},
                {$match:{
                    $or:[
                        {names:{$regex:parameters,$options:'g'}},    
                    ]
                }},
            ]
            res = Meteor.users.aggregate(query)
            return res
        }
    }, */
}) 

Meteor.publish('viewUser',function(parameters){
    if(parameters){
        let us = users.find(parameters.filters,parameters.projection)
        return us
    }
})

