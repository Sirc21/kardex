import {Class,Validator,Enum} from 'meteor/jagi:astronomy';
export const collectionKardex = new Mongo.Collection('kardex', {idGeneration: 'MONGO'});

export const kardex = Class.create({
    name : 'kardex',
    collection:collectionKardex,
    fields:{
        nameKardex:{
            type:String,
        },
        surname:{
        type:String,
        },
        CI:{
            type:Number,
            optional:true
        },
        RU:{
            type:Number,
            optional:true
        },
        /*IDRA:{
            type:String,
        },*/
        images:{
            type:[String],
            default:[],
            optional:true
        },
        admision:{
            type:String,
            optional:true
        },
        bachiller:{
            type:Boolean,
            optional:true
        },
        certnacimiento:{
            type:Boolean,
            optional:true
        },
        carnetidentidad:{
            type:Boolean,
            optional:true
        },
        certnotas:{
            type:[String],
            optional:true
        },
        matricula:{
            type:[String],
            optional:true
        },
    }
})
