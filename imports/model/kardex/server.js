import {kardex} from './class'
kardex.extend({
    meteorMethods:{
        createnewkardex(form){
            console.log(form)
            form.matricula = [form.matricula]
            console.log(form)
            form.certnotas = [form.certnotas]
            console.log(form)
            this.set(form)            
            return this.save()
        },
        editkardex(form){
            form.matricula = [form.matricula]
            form.certnotas = [form.certnotas]
            this.set(form)
            return this.save()
        },
        editQuantity(form){
            this.set(form)
            return this.save()
        },
        updateState(data){
            if(data.state == 'ENABLE'){
                this.state = 'DISABLE'
            }else{
                this.state = 'ENABLE'
            }
            return this.save()
        },
        newAlbum(album){
                const post = this
                if(post.images==null)
                    post.images=[]
                if(post.images.length>0){
                    album.forEach(element => 
                        post.images.push(element)
                    )

                    /*post.images.push(album)*/
                }
                else{
                    post.images = album
                }
                return post.save()
        },
        deleteImag(imagen){
            const post = this
            post.images.splice(imagen, 1);
            return post.save()
            console.log(post)
            console.log(imagen)
        },
        SumCertMatr(form){
            this.matricula.push(form.matricula)
            this.certnotas.push(form.certnotas)
            return this.save()
        },

    }
})

Meteor.publish('viewallKardex',function(parameters){
    if(parameters){
        let kx = kardex.find(parameters.filters,parameters.projection)
        return kx
    }
})
