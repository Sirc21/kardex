import { Template } from 'meteor/templating';
import {kardex} from '../../model/kardex/class'
import '../../view/kardex/viewKardex'

Template.viewKardex.onCreated(function(){
    let template = this
    template.viewAlbum = new ReactiveVar(undefined) 
    template.viewImages = new ReactiveVar(undefined) 
    template.asyncImagesAlbum = new ReactiveVar(undefined)
    template.viewKardexAll = new ReactiveVar(undefined)
    template.DeleteImag = new ReactiveVar(undefined)
    template.editKardex = new ReactiveVar(undefined)
    template.SumCertMatr = new ReactiveVar(undefined)
    template.IncompletComplet = new ReactiveVar(undefined)
    template.autorun(function(){
            parameters = {
                filters:{},
                projection:{}
            }
            template.postSubscription = template.subscribe('viewallKardex', parameters);
    });
});

function searchObjectForKey(Key,myArray,property){
    var resp;
    //sorted = _.sortBy(myArray, 'createdAt');
    //OrderArray = sorted.reverse();
    $.each(myArray,function(i,v){
        if(JSON.stringify(v[property]) === JSON.stringify(Key)){
            resp = v
            return false
        }
    })
    return resp
}


Template.viewKardex.helpers({
    getKardex: function () {
        let template = Template.instance()
        const ic = template.IncompletComplet.get();
        if(template.postSubscription.ready()){
            /*console.log(kardex.find({}).fetch())*/
            let resp = kardex.find({})
            console.log(resp.fetch())
            let check = true   
            let i = []
            let c = []
            resp.forEach(element => {
                for (const prop in element) {
                    if(typeof element[prop] === 'undefined'){
                        check = false
                        i.push(element)
                        break
                    }
                }
                let xc = searchObjectForKey(element._id,i,'_id')
                if(!xc){
                    c.push(element)
                }
            });
            console.log(ic)
            if(!ic){
                return resp
            }else{
                if(ic=='complet'){
                    return c
                }
                if(ic=='incomplet'){
                    return i
                }
            }
            
        }
    },
    imagesView:()=>{
        let template = Template.instance()
        const resp = template.viewImages.get();
        if(resp)
            return resp
    },
    stateKardex:(element)=>{
        let check = true
        for (const prop in element) {
            if(typeof element[prop] === 'undefined'){
                check = false
                break
            }
        }
        return check?'<span class="badge badge-pill fl-r badge-success lh-0 p-10">COMPLETO</span>':'<span class="badge badge-pill fl-r badge-danger lh-0 p-10">PENDIENTE</span>'
    },
    ViewKardexAll:()=>{
        let template = Template.instance()
        const resp = template.viewKardexAll.get();
        
        if(resp){ 
                /*if(resp.bachiller)
                    resp.bachillerview="Presentado"
                else
                    resp.bachillerview="Pendiente"
                if(resp.certnacimiento)
                    resp.certnacimientoview="Presentado"
                else
                    resp.certnacimientoview="Pendiente"
                if(resp.carnetidentidad)
                    resp.carnetidentidadview="Presentado"
                else
                    resp.carnetidentidadview="Pendiente"
                console.log(resp)*/
                return resp
 
        }
            
    },
    checkPresent:(data)=>{
        if(data)
            return "Presentado"
        else
            return "Pendiente"
    },
    editKardex:()=>{
        let template = Template.instance()
        const resp = template.editKardex.get();
        console.log(resp)
        if(resp)
            return resp
    },
    checksec:(d1,d2)=>{
        if(d1 == d2)
            return "selected"
    },
    index:(i)=>{
        return i+1
    }
});


function resizeImages(file,w,h,quality,complete) {
    var reader = new FileReader();
      reader.onload = function(e) {
          var img = new Image();
          img.onload = function() {
            complete(resizeInCanvasImg(img,w,h,file.type,quality));
          };
          img.src = e.target.result;
        }
      reader.readAsDataURL(file);
   
}

function resizeInCanvasImg(img,w,h,type,quality){
    //var perferedWidth = size;
    //var ratio = perferedWidth / img.width;
    var canvas = $("<canvas>")[0];
    canvas.width = w
    canvas.height = h
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0,0,canvas.width, canvas.height);
    return canvas.toDataURL(type,quality);
}



Template.viewKardex.events({ 
    'click .viewAlbum': function(event,template){
        event.preventDefault();
        template.viewAlbum.set(this)
        $('#myModal').modal('toggle')
    },
    'change #imagesAlbum': function(event,template){
        $( ".gallery" ).html('');
        const files = document.querySelector('#imagesAlbum').files
            if (files) {
                var filesAmount = files.length;
        
                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        let img =   '<div class="col-sm-4 col-xs-6 col-md-3 col-lg-3">'+
                                        '<a class="thumbnail fancybox" rel="ligthbox">'+
                                            '<img class="img-responsive" alt="" src="'+e.target.result+'" style="width: 306px;height: 360px;"/>'+
                                            '<div class="text-right">'+
                                                '<small class="text-muted">Nueva Imagen</small>'+
                                            '</div>'+
                                        '</a>'+
                                   '</div>'
                        $( ".gallery" ).append(img);
                    }
                    reader.readAsDataURL(files[i]);
                }
            }
    },
    'submit #newalbum': function(event,template){
        event.preventDefault();
        let form = $(event.currentTarget);
        form = form.serializeJSON({parseNumbers: true});
        let w = 320
        let h = 215
        quality = 0.9
        const files = document.querySelector('#imagesAlbum').files
            if (files) {
                var filesAmount = files.length;
                //////////////////////////////////////////////////////////
                function asyncLoop(iterations, func, callback) {
                    var index = 0;
                    var done = false;
                    var loop = {
                        next: function() {
                            if (done) {
                                return;
                            }

                            if (index < iterations) {
                                resizeImages(files[index],w,h,quality,function(dataUrl) {
                                    let arrayAlbum = template.asyncImagesAlbum.get()
                                    if(arrayAlbum){
                                        arrayAlbum.push(dataUrl)
                                        template.asyncImagesAlbum.set(arrayAlbum)
                                    }
                                    else{
                                        arrayAlbum = [dataUrl]
                                        template.asyncImagesAlbum.set(arrayAlbum)
                                    }
                                    index++;
                                    func(loop);
                                })

                            } else {
                                done = true;
                                callback();
                            }
                        },

                        iteration: function() {
                            return index - 1;
                        },

                        break: function() {
                            done = true;
                            callback();
                        }
                    };
                    loop.next()
                    return loop
                }

                function someFunction(callback) {
                    callback()
                }

                asyncLoop(filesAmount, function(loop) {
                    someFunction(function(result) {
                        loop.iteration()
                        loop.next()
                    })},
                    function(){
                        const bgs = template.viewAlbum.get()
                        //const ab = new albumBlog()
                        //ab.title = form.title
                        const ab = template.asyncImagesAlbum.get()
                        console.log(template.viewAlbum.get())
                        console.log(template.asyncImagesAlbum.get())
                        bgs.callMethod('newAlbum',ab,(error,result)=>{
                            if(error){
                                console.log(error)
                                swal({
                                  icon: "error",
                                  text: error.reason
                                });
                            }
                            else{
                                swal({
                                    icon: "success",
                                    text: "Creado con exito",
                                    timer: 2000,
                                    buttons: false,
                                })
                                template.asyncImagesAlbum.set(undefined)
                                $('#newalbum')[0].reset()
                                $('.gallery').html('')
                            }
                        })
                    }
                );
                //////////////////////////////////////////////////////////
            }
    },
    'click .view': function(event,template){
        event.preventDefault();
        console.log(this)
        template.viewImages.set(this.images)
        template.DeleteImag.set(this)
        $('#myModalViewImages').modal('toggle')
    },

    'click .viewKardexAll': function(event,template){
        event.preventDefault();
        console.log(this)
        template.viewKardexAll.set(this)
        $('#myModalViewKardex').modal('toggle')
    },
    'click .DeleteImag': function(event,template){
        event.preventDefault();
        console.log(template.DeleteImag.get())
        let index = $(event.currentTarget).attr('rca')
        console.log(index)
        let kar = template.DeleteImag.get()
        kar.callMethod('deleteImag',index,(error,result)=>{
            if(error){
                console.log(error)
                swal({
                  icon: "error",
                  text: error.reason
                });
            }
            else{
                swal({
                    icon: "success",
                    text: "eliminado con exito",
                    timer: 2000,
                    buttons: false,
                })
                $('#myModalViewImages').modal('toggle')
            }
        })
    },
     
    'submit #editkardexform': function(event, template) {
    event.preventDefault();
    let form = $(event.currentTarget);
    form = form.serializeJSON({parseNumbers: true});
        if (form.bachiller==1){
            form.bachiller=true
        }
        else{
            form.bachiller=undefined
        }
        if (form.certnacimiento==1){
            form.certnacimiento=true
        }
        else{
            form.certnacimiento=undefined
        }
        if (form.carnetidentidad==1){
            form.carnetidentidad=true
        }
        else{
            form.carnetidentidad=undefined
        }
        console.log(form)
        editkardex = template.editKardex.get()
    console.log(editkardex)
    editkardex.callMethod('editkardex',form,(error,result)=>{
        if(error != undefined){
            swal('Error!',error.reason,'error')
        }
        else{
            swal('Exito!','kardex editado correctamente!','success')
            $("form")[0].reset();
        }
    })
    return false
    },
    'click .editKardex': function(event,template){
        event.preventDefault();
        template.editKardex.set(this)
        console.log(this)
        $('#myModaleditKardex').modal('toggle')
    },
    

    'submit #SumCertMatr': function(event,template){
        event.preventDefault();
        let form = $(event.currentTarget);
        form = form.serializeJSON({parseNumbers: true});
        console.log(form)
        
                        const ab = template.SumCertMatr.get()
                        console.log(template.SumCertMatr.get())
                        ab.callMethod('SumCertMatr',form,(error,result)=>{
                            if(error){
                                console.log(error)
                                swal({
                                  icon: "error",
                                  text: error.reason
                                });
                            }
                            else{
                                swal({
                                    icon: "success",
                                    text: "se Añadio con exito",
                                    timer: 2000,
                                    buttons: false,
                                })
                                template.SumCertMatr.set(undefined)
                                
                            }
                        })
                
    },
    
    'click .editKardexCertMat': function(event,template){
        event.preventDefault();
       template.SumCertMatr.set(this)
        console.log(this)
        $('#myModaleditCM').modal('toggle')
    },

    'click .incomplet': function(event,template){
        event.preventDefault();
        template.IncompletComplet.set('incomplet')
        console.log('incomplet')
    },
    'click .complet': function(event,template){
        event.preventDefault();
        template.IncompletComplet.set('complet')
        console.log('complet')
    },
    'click .allIC': function(event,template){
        event.preventDefault();
        template.IncompletComplet.set(undefined)
    },
    'keyup #myInput': function(event,template){
        console.log($(event.currentTarget).val())

        var input, filter, table, tr, td, i, txtValue;
        input = $(event.currentTarget).val()
        filter = input.toUpperCase();
        table = document.getElementById("dataTable");
        tr = table.getElementsByTagName("tr");

        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("th")[4];
            console.log(td)
            if (td) {
              txtValue = td.textContent || td.innerText;
              console.log(txtValue)
              if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
              } else {
                tr[i].style.display = "none";
              }
            }
        }
    },
    'click .reportFormDate':function(event,template){
        event.preventDefault()
        console.log(this)
        let context = this
        var doc = new jsPDF()
        doc.setFontSize(16)
        doc.text(55, 10, 'INFORMACIÓN DEL KARDEX ESTUDIANTIL')
        doc.text(20, 20, 'Nombres:')
        doc.text(40, 30, context.nameKardex.toString())
        doc.text(20, 40, 'Apellidos:')
        doc.text(40, 50, context.surname.toString())
        doc.text(20, 60, 'Numero de carnet de identidad:')
        doc.text(40, 70, context.CI.toString())
        doc.text(20, 80, 'Registro Universitario:')
        doc.text(40, 90, context.RU.toString())
        doc.text(20, 100, 'Modalidad de Ingreso:')
        doc.text(40, 110, context.admision.toString())
        doc.text(20, 120, 'Titulo de Bachiller:')
        doc.text(40, 130, (context.bachiller)?'presentado':'pendiente')
        doc.text(20, 140, 'Certificado de Nacimiento:')
        doc.text(40, 150, (context.certnacimiento)?'presentado':'pendiente')
        doc.text(20, 160, 'Carnet de identidad:')
        doc.text(40, 170, (context.carnetidentidad)?'presentado':'pendiente')
        doc.text(20, 180, 'Certificado de Notas:')
        doc.text(40, 190, context.certnotas)
        doc.text(20, 200, 'Matricula:')
        doc.text(40, 210, context.matricula)
        context.images.forEach(element => {
            doc.addPage()
            doc.addImage(element, 'JPEG', 15, 20, 180, 160)
        })


        doc.save('Reporte.pdf');
        /*var columns = [
            {title: "Nombres", key: "nameKardex"}, 
            {title: "Apellidos", key: "surname"}, 
            {title: "Numero de carnet de identidad", key: "CI"}, 
            {title: "Registro Universitario", key: "RU"}, 
            {title: "Modalidad de Ingreso", key: "admision"},
            {title: "Titulo de Bachiller", key: "checkPresent bachiller"},
            {title: "Certificado de Nacimiento", key: "checkPresent certnacimiento"},
            {title: "Carnet de identidad", key: "checkPresent carnetidentidad"},
            {title: "Certificado de Notas", key: "certnotas"},
            {title: "Matricula", key: "matricula"},
        ];
        
        var doc = new jsPDF('l','pt');
        let finalY = doc.autoTableEndPosY();
        doc.text('Reporte Inventarios por fechas de precios',40, finalY + 45);
        doc.setFontSize(9);
        doc.text('Impreso el: '+moment(new Date).format('DD/MM/YYYY HH:mm'), 685, finalY + 47);*/

        /*$.each(Session.get('inventoryDate'),function(i,v){           
            var data = []
            $.each(v,function(ii,vv){
                let item = {
                    nameKardex:vv.nameKardex,
                    surname:vv.surname,
                    CI:vv.CI,
                    RU:vv.RU,
                    admision:vv.admision,
                    checkPresent bachiller:vv.checkPresent bachiller,
                    checkPresent certnacimiento:vv.checkPresent certnacimiento,
                    checkPresent carnetidentidad:vv.checkPresent carnetidentidad,
                    certnotas:vv.certnotas,
                    matricula:vv.matricula
                }
                data.push(item)
            })
            let finish = {
                nameKardex:'TOTALES',
                surname:'-',
                CI:'-',
                RU:'-',
                admision:'Total',
                checkPresent bachille:'Total: ',
                checkPresent certnacimiento:'Total: ',
                checkPresent carnetidentidad:'Total: ',
                certnotas:'Total: ',
                matricula:'Totall: '
            }
            data.push(finish)
            doc.autoTable(columns, data, {
                startY: doc.autoTableEndPosY() + 70
            });
        })*/
        //var doc = new jsPDF('l','pt');
        //doc.autoTable(columns, datas, {});
        //doc.save('Reporte.pdf');
    }

    
})


function myFunction() {
  // Declare variables
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("dataTable");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
