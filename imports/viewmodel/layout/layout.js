import { Template } from 'meteor/templating';
import '../../view/layout/home'
Template.layout.events({
    'click .logout': function(event){
        event.preventDefault();
        Meteor.logout();
        Meteor._localStorage.removeItem('Meteor.loginToken');
        Meteor._localStorage.removeItem('Meteor.loginTokenExpires');
        Meteor._localStorage.removeItem('Meteor.userId');
        location.reload();
    }
});
    