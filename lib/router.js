/*FlowRouter.route('/', {
    action: function(params, queryParams) {
        BlazeLayout.render('layout',{main:'login'});
    },
});*/


FlowRouter.route('/', {
    name:"login",
    action: function(params, queryParams) {
        BlazeLayout.render('login');
    },
});

FlowRouter.route('/signup', {
    name:"signup",
    action: function(params, queryParams) {
        if(Meteor.userId()){
            BlazeLayout.render('layout',{main:'signup'});
        }else{
            FlowRouter.go('login')
        }
    },
});

FlowRouter.route('/home', {
    name:"home",
    action: function(params, queryParams) {
        if(Meteor.userId()){
            BlazeLayout.render('layout',{main:'home'});
        }else{
            FlowRouter.go('login')
        }
    },
});

FlowRouter.route('/register', {
    name:"register",
    action: function(params, queryParams) {
        if(Meteor.userId()){
            BlazeLayout.render('layout',{main:'register_kardex'});
        }else{
            FlowRouter.go('login')
        }
    },
});

FlowRouter.route('/viewKardex', {
    name:"viewKardex",
    action: function(params, queryParams) {
        if(Meteor.userId()){
            BlazeLayout.render('layout',{main:'viewKardex'});
        }else{
            FlowRouter.go('login')
        }
    },
});

FlowRouter.route('/viewUser', {
    name:"viewUser",
    action: function(params, queryParams) {
        if(Meteor.userId()){
            BlazeLayout.render('layout',{main:'viewUser'});
        }else{
            FlowRouter.go('login')
        }
    },
});